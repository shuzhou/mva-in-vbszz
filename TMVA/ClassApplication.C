/**********************************************************************************
 * Project   : TMVA - a Root-integrated toolkit for multivariate data analysis    *
 * Package   : TMVA                                                               *
 * Exectuable: ClassApplication                                                   *
 *                                                                                *
 * Test suit for comparison of Reader and standalone class outputs                *
 **********************************************************************************/

#include <vector>
//class ReadLikelihoodPCA;
//class ReadLikelihoodMIX;
//class ReadHMatrix;
//class ReadFisherG;
//class ReadLD;
//class ReadFDA_MT;
//class ReadFDA_MC;
//class ReadFDA_GA;
//class ReadMLP;
//class ReadMLPBFGS;
//class ReadBDT;
//class ReadBDTD;
class ReadBDTG;
//class ReadBDTB;

//void ClassApplication( TString myMethodList = "Fisher" ) 
void ClassApplication( TString myMethodList = "BDTG" ) 
{
  cout << endl;
  cout << "==> start ClassApplication" << endl;
  //const int Nmvas = 16;
  const int Nmvas = 1;

  //const char* bulkname[Nmvas] = { "MLP","MLPBFGS","Fisher","FisherG","Likelihood","LikelihoodD","LikelihoodPCA","LD","HMatrix","FDA_MT","FDA_MC","FDA_GA","BDT","BDTD","BDTG","BDTB"};
  const char* bulkname[Nmvas] = { "BDTG"};

  bool iuse[Nmvas] = { Nmvas*kFALSE };

  // interpret input list
  if (myMethodList != "") {
    TList* mlist = TMVA::gTools().ParseFormatLine( myMethodList, " :," );
    for (int imva=0; imva<Nmvas; imva++) if (mlist->FindObject( bulkname[imva] )) iuse[imva] = kTRUE;
    delete mlist;
  }

  // create a set of variables and declare them to the reader
  // - the variable names must corresponds in name and type to 
  // those given in the weight file(s) that you use
  std::vector<std::string> inputVars;
  //inputVars.push_back( "var1+var2" );
  //inputVars.push_back( "var1-var2" );
  //inputVars.push_back( "var3" );
  //inputVars.push_back( "var4" );

  inputVars.push_back( "MJJ" );
  inputVars.push_back( "MZZ" );
  //inputVars.push_back( "dEtaJJ" );
  inputVars.push_back( "dYJJ" );
 // inputVars.push_back( "dPhiJJ" );
  inputVars.push_back( "PtZZ" );
  inputVars.push_back( "PtZ1" );
  //inputVars.push_back( "PtZ2" );
  inputVars.push_back( "PtJ1" );
  inputVars.push_back( "PtJ2" );
 // inputVars.push_back( "EtaJ1" ); changed on 0314
 // inputVars.push_back( "EtaJ2" );
  inputVars.push_back( "YJ1xJ2" );
//  inputVars.push_back( "PtL1" ); changed on 0314
 // inputVars.push_back( "PtL2" );
 inputVars.push_back( "PtL3" );//changed on 0314
 // inputVars.push_back( "PtL4" );
  //inputVars.push_back( "dEtaL1L2" );
  //inputVars.push_back( "dEtaL1L3" );
  //inputVars.push_back( "dEtaL1L4" );
  //inputVars.push_back( "dEtaL2L3" );
  //inputVars.push_back( "dEtaL2L4" );
  //inputVars.push_back( "dEtaL3L4" );
  //inputVars.push_back( "dPtJJoPtJJ" );
 // inputVars.push_back( "nJet" );
 // inputVars.push_back( "Z1Cen" );
  //inputVars.push_back( "Z2Cen" );
  //inputVars.push_back( "dEtaZZ" );
 // inputVars.push_back( "EtaZ1xZ2" );
  inputVars.push_back( "YZ1Star" );
  inputVars.push_back( "YZ2Star" );
 // inputVars.push_back( "PtJJOHtJJ" );
  inputVars.push_back( "PtZZJJOHtZZJJ" );
 
  // preload standalone class(es)
  string dir    = "weights/";
  string prefix = "TMVAClassification";

  for (int imva=0; imva<Nmvas; imva++) {
    if (iuse[imva]) {
      TString cfile = dir + prefix + "_" + bulkname[imva] + ".class.C++";

      cout << "=== Macro        : Loading class  file: " << cfile << endl;         

      // load the classifier's standalone class
      gROOT->LoadMacro( cfile );
    }
  }
  cout << "=== Macro        : Classifier class loading successfully terminated" << endl;

  // define classes
  IClassifierReader* classReader[Nmvas] = { Nmvas*0 };

  // ... and create them (and some histograms for the output)
  int nbin = 100;
  TH1* hist[Nmvas];

  for (int imva=0; imva<Nmvas; imva++) {
    if (iuse[imva]) {
      cout << "=== Macro        : Testing " << bulkname[imva] << endl;
      //if (bulkname[imva] == "Likelihood"   ) {
      //  classReader[imva] = new ReadLikelihood   ( inputVars );            
      //  hist[imva] = new TH1F( "MVA_Likelihood",    "MVA_Likelihood",    nbin,  0, 1 );
      //}
      //if (bulkname[imva] == "LikelihoodD"  ) {
      //  classReader[imva] = new ReadLikelihoodD  ( inputVars );
      //  hist[imva] = new TH1F( "MVA_LikelihoodD",   "MVA_LikelihoodD",   nbin,  0, 1 );
      //}
      //if (bulkname[imva] == "LikelihoodPCA") {
      //  classReader[imva] = new ReadLikelihoodPCA( inputVars );
      //  hist[imva] = new TH1F( "MVA_LikelihoodPCA", "MVA_LikelihoodPCA", nbin,  0, 1 );
      //}
      //if (bulkname[imva] == "LikelihoodMIX") {
      //  classReader[imva] = new ReadLikelihoodMIX( inputVars );
      //  hist[imva] = new TH1F( "MVA_LikelihoodMIX", "MVA_LikelihoodMIX", nbin,  0, 1 );
      //}
      //if (bulkname[imva] == "HMatrix"      ) {
      //  classReader[imva] = new ReadHMatrix      ( inputVars );
      //  hist[imva] = new TH1F( "MVA_HMatrix",       "MVA_HMatrix",       nbin, -0.95, 1.55 );
      //}
      //if (bulkname[imva] == "Fisher"       ) {
      //  classReader[imva] = new ReadFisher       ( inputVars );
      //  hist[imva] = new TH1F( "MVA_Fisher",        "MVA_Fisher",        nbin, -4, 4 );
      //}
      //if (bulkname[imva] == "FisherG"       ) {
      //  classReader[imva] = new ReadFisherG       ( inputVars );
      //  hist[imva] = new TH1F( "MVA_FisherG",        "MVA_FisherG",        nbin, -4, 4 );
      //}
      //if (bulkname[imva] == "LD"   ) {
      //  classReader[imva] = new ReadLD   ( inputVars );            
      //  hist[imva] = new TH1F( "MVA_LD",    "MVA_LD",    nbin,  -1., 1 );
      //}	 
      //if (bulkname[imva] == "FDA_MT"       ) {
      //  classReader[imva] = new ReadFDA_MT       ( inputVars );
      //  hist[imva] = new TH1F( "MVA_FDA_MT",        "MVA_FDA_MT",        nbin, -2.0, 3.0 );
      //}
      //if (bulkname[imva] == "FDA_MC"       ) {
      //  classReader[imva] = new ReadFDA_MC       ( inputVars );
      //  hist[imva] = new TH1F( "MVA_FDA_MC",        "MVA_FDA_MC",        nbin, -2.0, 3.0 );
      //}
      //if (bulkname[imva] == "FDA_GA"       ) {
      //  classReader[imva] = new ReadFDA_GA       ( inputVars );
      //  hist[imva] = new TH1F( "MVA_FDA_GA",        "MVA_FDA_GA",        nbin, -2.0, 3.0 );
      //}
      //if (bulkname[imva] == "MLP"          ) {
      //  classReader[imva] = new ReadMLP          ( inputVars );
      //  hist[imva] = new TH1F( "MVA_MLP",           "MVA_MLP",           nbin, -1.2, 1.2 );
      //}
      //if (bulkname[imva] == "MLPBFGS"          ) {
      //  classReader[imva] = new ReadMLPBFGS          ( inputVars );
      //  hist[imva] = new TH1F( "MVA_MLPBFGS",           "MVA_MLPBFGS",           nbin, -1.5, 1.5 );
      //}
      //if (bulkname[imva] == "BDT"          ) {
      //  classReader[imva] = new ReadBDT          ( inputVars );
      //  hist[imva] = new TH1F( "MVA_BDT",           "MVA_BDT",           nbin, -1, 1 );
      //}
      //if (bulkname[imva] == "BDTD"          ) {
      //  classReader[imva] = new ReadBDTD          ( inputVars );
      //  hist[imva] = new TH1F( "MVA_BDTD",           "MVA_BDTD",         nbin, -1, 1 );
      //}
      if (bulkname[imva] == "BDTG"          ) {
       classReader[imva] = new ReadBDTG          ( inputVars );
       hist[imva] = new TH1F( "MVA_BDTG",           "MVA_BDTG",         nbin, -1, 1 );
      }
      //if (bulkname[imva] == "BDTB"          ) {
      //  classReader[imva] = new ReadBDTB          ( inputVars );
      //  hist[imva] = new TH1F( "MVA_BDTB",           "MVA_BDTB",         nbin, -1, 1 );
      //}
    }
  }
  cout << "=== Macro        : Class creation was successful" << endl;

  // Prepare input tree (this must be replaced by your data source)
  // in this example, there is a toy tree with signal and one with background events
  // we'll later on use only the "signal" events for the test in this example.
  //   
  TFile *input(0);
  //if (!gSystem->AccessPathName("./tmva_example.root")) {
  //  // first we try to find tmva_example.root in the local directory
  //  cout << "=== Macro        : Accessing ./tmva_example.root" << endl;
  //  input = TFile::Open("tmva_example.root");
  //} 

  input = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/BDT-truth-0517/mc16_13TeV.345706.Sherpa_222_NNPDF30NNLO_ggllll_130M4l_SR.root","update");

  if (!input) {
    cout << "ERROR: could not open data file" << endl;
    exit(1);
  }
  ifstream in1;
  in1.open("/atlas/data18a/shuzhouz/VBSZZ4l/Code_Rel21/source/MyAnalysis/share/tree_name2.txt",ios::out);
  string name;
  // prepare the tree
  // - here the variable names have to corresponds to your tree
  // - you can use the same variables as above which is slightly faster,
  //   but of course you can use different ones and copy the values inside the event loop
  //
  //TTree* theTree = (TTree*)input->Get("TreeS");
  while(in1>>name){
  cout<<"processsing: "<<name<<endl;
  TTree* theTree;
  input->GetObject(name.c_str(),theTree);
  cout << "=== Macro        : Loop over signal sample" << endl;
  Double_t BDT;
  TBranch *bBDT = theTree->Branch("BDT",&BDT,"BDT/D");
  theTree->SetBranchAddress("BDT",&BDT);

  // the references to the variables
  //float var1, var2, var3, var4;
  //float userVar1, userVar2;
  //theTree->SetBranchAddress( "var1", &userVar1 );
  //theTree->SetBranchAddress( "var2", &userVar2 );
  //theTree->SetBranchAddress( "var3", &var3 );
  //theTree->SetBranchAddress( "var4", &var4 );

  // variables
  Int_t           run;
  ULong64_t       event;
  Double_t        weight;
  Double_t        PtL1;
  Double_t        PtL2;
  Double_t        PtL3;
  Double_t        PtL4;
  Double_t        EtaL1;
  Double_t        EtaL2;
  Double_t        EtaL3;
  Double_t        EtaL4;
  Double_t        PhiL1;
  Double_t        PhiL2;
  Double_t        PhiL3;
  Double_t        PhiL4;
  Int_t           PIDL1;
  Int_t           PIDL2;
  Int_t           PIDL3;
  Int_t           PIDL4;
  Double_t        PtJ1;
  Double_t        PtJ2;
  Double_t        EtaJ1;
  Double_t        EtaJ2;
  Double_t        PhiJ1;
  Double_t        PhiJ2;
  Double_t        MJ1;
  Double_t        MJ2;
  Int_t           nJet;
  Int_t           nLep;
  Double_t        MZ1;
  Double_t        PtZ1;
  Double_t        MZ2;
  Double_t        PtZ2;
  Double_t        MZZ;
  Double_t        PtZZ;
  Double_t        dYJJ;
  Double_t        dPhiJJ;
  Double_t        MJJ;
  Double_t        EtaJ1xJ2;
  Double_t        YZ1Star;
  Double_t        YZ2Star;
  Double_t        PtJJOHtJJ;
  Double_t        PtZZJJOHtZZJJ;
  Double_t        Z1Cen;
  Double_t        Z2Cen;
  Double_t        dEtaZZ;
  Double_t        EtaZ1xZ2;
  Double_t        YJ1xJ2;
  // Set branch addresses.
 theTree->SetBranchAddress("weight",&weight);
 //theTree->SetBranchAddress("PtL1",&PtL1);
 //theTree->SetBranchAddress("PtL2",&PtL2);
 theTree->SetBranchAddress("PtL3",&PtL3);
 //theTree->SetBranchAddress("PtL4",&PtL4);
 //theTree->SetBranchAddress("EtaL1",&EtaL1);
 //theTree->SetBranchAddress("EtaL2",&EtaL2);
// theTree->SetBranchAddress("EtaL3",&EtaL3);
// theTree->SetBranchAddress("EtaL4",&EtaL4);
 //theTree->SetBranchAddress("PhiL1",&PhiL1);
 //theTree->SetBranchAddress("PhiL2",&PhiL2);
 //theTree->SetBranchAddress("PhiL3",&PhiL3);
 //theTree->SetBranchAddress("PhiL4",&PhiL4);
// theTree->SetBranchAddress("PIDL1",&PIDL1);
// theTree->SetBranchAddress("PIDL2",&PIDL2);
 //theTree->SetBranchAddress("PIDL3",&PIDL3);
 //theTree->SetBranchAddress("PIDL4",&PIDL4);
 theTree->SetBranchAddress("PtJ1",&PtJ1);
 theTree->SetBranchAddress("PtJ2",&PtJ2);
 theTree->SetBranchAddress("EtaJ1",&EtaJ1);
 theTree->SetBranchAddress("EtaJ2",&EtaJ2);
 //theTree->SetBranchAddress("PhiJ1",&PhiJ1);
 //theTree->SetBranchAddress("PhiJ2",&PhiJ2);
 theTree->SetBranchAddress("MJ1",&MJ1);
 theTree->SetBranchAddress("MJ2",&MJ2);
// theTree->SetBranchAddress("nJet",&nJet);
 //theTree->SetBranchAddress("nLep",&nLep);
 theTree->SetBranchAddress("MZ1",&MZ1);
 theTree->SetBranchAddress("PtZ1",&PtZ1);
 //theTree->SetBranchAddress("MZ2",&MZ2);
 //theTree->SetBranchAddress("PtZ2",&PtZ2);
 theTree->SetBranchAddress("MZZ",&MZZ);
 theTree->SetBranchAddress("PtZZ",&PtZZ);
 theTree->SetBranchAddress("dYJJ",&dYJJ);
 //theTree->SetBranchAddress("dPhiJJ",&dPhiJJ);
 theTree->SetBranchAddress("MJJ",&MJJ);
 theTree->SetBranchAddress("YJ1xJ2",&YJ1xJ2);
 theTree->SetBranchAddress("YZ1Star",&YZ1Star);
 theTree->SetBranchAddress("YZ2Star",&YZ2Star);
 //theTree->SetBranchAddress("PtJJOHtJJ",&PtJJOHtJJ);
 theTree->SetBranchAddress("PtZZJJOHtZZJJ",&PtZZJJOHtZZJJ);
 //theTree->SetBranchAddress("Z1Cen",&Z1Cen);
 //theTree->SetBranchAddress("Z2Cen",&Z2Cen);
 //theTree->SetBranchAddress("dEtaZZ",&dEtaZZ);
 //theTree->SetBranchAddress("EtaZ1xZ2",&EtaZ1xZ2);
  cout << "=== Macro        : Processing total of " << theTree->GetEntries() << " events ... " << endl;

  //std::vector<double>* inputVec = new std::vector<double>( 30 );
  std::vector<double>* inputVec = new std::vector<double>( 12 );
  for (Long64_t ievt=0; ievt<theTree->GetEntries();ievt++) {

    if (ievt%100 == 0) cout << "=== Macro        : ... processing event: " << ievt << endl;

    theTree->GetEntry(ievt);

    //var1 = userVar1 + userVar2;
    //var2 = userVar1 - userVar2;

    //(*inputVec)[0] = var1;
    //(*inputVec)[1] = var2;
    //(*inputVec)[2] = var3;
    //(*inputVec)[3] = var4;
    (*inputVec)[0]  = MJJ;
    (*inputVec)[1]  = MZZ;
    (*inputVec)[2]  = fabs(dYJJ);
 //   (*inputVec)[3]  = fabs(dPhiJJ); changed on 0225
    (*inputVec)[3]  = PtZZ;
    (*inputVec)[4]  = PtZ1;
   // (*inputVec)[5]  = PtZ2;
    (*inputVec)[5]  = PtJ1;
    (*inputVec)[6]  = PtJ2;
 //   (*inputVec)[8]  = EtaJ1;
   // (*inputVec)[10] = EtaJ2; changed on 0225
    (*inputVec)[7] = YJ1xJ2;
   // (*inputVec)[10] = PtL1;
   // (*inputVec)[13] = PtL2; changed on 0225
    (*inputVec)[8] = PtL3;
   // (*inputVec)[15] = PtL4;
    //(*inputVec)[16] = fabs(EtaL1-EtaL2);
    //(*inputVec)[17] = fabs(EtaL1-EtaL3);
    //(*inputVec)[18] = fabs(EtaL1-EtaL4);
    //(*inputVec)[19] = fabs(EtaL2-EtaL3);
    //(*inputVec)[20] = fabs(EtaL2-EtaL4);
    //(*inputVec)[21] = fabs(EtaL3-EtaL4);
    //(*inputVec)[22] = (PtJ1-PtJ2)/(PtJ1+PtJ2);
   // (*inputVec)[16] = (double) nJet;
   // (*inputVec)[16] = Z1Cen;
    //(*inputVec)[17] = Z2Cen;
   // (*inputVec)[18] = dEtaZZ;
   // (*inputVec)[19] = EtaZ1xZ2;
    (*inputVec)[9] = YZ1Star;
    (*inputVec)[10] = YZ2Star;
   // (*inputVec)[18] = PtJJOHtJJ; changed on0225
    (*inputVec)[11] = PtZZJJOHtZZJJ;
    // loop over all booked classifiers
    BDT = -999.; // initial value
    for (int imva=0; imva<Nmvas; imva++) {
      if(MJJ < 0.) continue;

     if (iuse[imva]) {

        // retrive the classifier responses            
     // if((MJJ>300.)&&(fabs(dEtaJJ)>2.)){
        double retval = classReader[imva]->GetMvaValue( *inputVec );
       // hist[imva]->Fill( retval, weight*0.000175 );
        BDT = retval;
      }
     }
   // }
    bBDT->Fill();
  }

  theTree->Write("", TObject::kOverwrite);
//  input->Close();
 // delete input;

  cout << "=== Macro        : Event loop done! " << endl;

 // TFile *target  = new TFile( "ClassApp.root","RECREATE" );
 // for (int imva=0; imva<Nmvas; imva++) {
   // if (iuse[imva]) {
    //  hist[imva]->Write();
   // }
 // }
 // cout << "=== Macro        : Created target file: " << target->GetName() << endl;
 // target->Close();
 
 // delete target;
 
  delete inputVec;
  }
  input->Close();
  delete input;
  cout << "==> ClassApplication is done!" << endl << endl;
} 
